<h2>Join an Atlassian User Group</h2>

Where Atlassian users and developers meet and share best practices. <br> <br><a href="http://aug.atlassian.com">Start connecting <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
